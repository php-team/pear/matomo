<?php

// Require
require_once '/usr/share/php/matomo/device-detector/autoload.php';
require_once '/usr/share/php/matomo/network/autoload.php';
require_once '/usr/share/php/Composer/CaBundle/autoload.php';
require_once '/usr/share/php/Composer/Semver/autoload.php';
require_once '/usr/share/php/JShrink/Minifier.php';
require_once '/usr/share/php/Monolog/autoload.php';
require_once '/usr/share/php/PEAR/Exception.php';
require_once '/usr/share/php/PhpDi/autoload.php';
require_once '/usr/share/php/Psr/Log/autoload.php';
require_once '/usr/share/php/Symfony/Bridge/Monolog/autoload.php';
require_once '/usr/share/php/Symfony/Component/Console/autoload.php';
require_once '/usr/share/php/Symfony/Component/EventDispatcher/autoload.php';
require_once '/usr/share/php/Twig/autoload.php';
require_once '/usr/share/php/Wikimedia/LessPhp/autoload.php';
require_once '/usr/share/php/libphp-phpmailer/autoload.php';
require_once '/usr/share/matomo/libs/ChartLib/ChartLib.php';
require_once '/usr/share/php/matomo/cache/autoload.php';
require_once '/usr/share/php/matomo/decompress/autoload.php';
require_once '/usr/share/php/matomo/ini/autoload.php';
require_once '/usr/share/php/matomo/matomo-php-tracker/autoload.php';
require_once '/usr/share/php/sparkline/autoload.php';
require_once '/usr/share/php/spyc/Spyc.php';
require_once '/usr/share/php/tcpdf/autoload.php';

// Suggest

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
// this is an autogenerated file - do not edit
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                ___CLASSLIST___
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require ___BASEDIR___$classes[$cn];
        }
    },
    ___EXCEPTION___,
    ___PREPEND___
);
// @codeCoverageIgnoreEnd

// Files
require_once '/usr/share/matomo/LegacyAutoloader.php';
