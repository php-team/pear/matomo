# https://github.com/matomo-org/docker/issues/77#issuecomment-2629162321
*/20 * * * * php -f /usr/share/matomo/console scheduled-tasks:run --quiet --no-interaction
